/* The file TROUGAO.TXT has data on a variety of triangles.
Each row of the file contains data on a triangle in the form of: (x1, y1) (x2, y2), (x3, y3) where,
(x1, y1), (x2, y2) and (x3, y3) - integer coordinates of the vertices of a triangle, for example:
	(0,0) (4,0) (4,3)
	(1,0) (1,1) (2,4)
Write a program that reads from a file TRIANGLE.TXT data and finds the triangle with the largest surface area
and the triangle with the largest circumference, and store the result in a file MEASURE.TXT. */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#define power(X) (X)*(X)
#define check(A,B) ((A)<(B) ? 1: 0)

typedef struct { int x, y; }TrianglePoint;
typedef struct { TrianglePoint p1, p2, p3; double area, circumference; }TRIANGLE;

TRIANGLE maxCircumference;

void mergeSort(TRIANGLE array[], int begin, int end)
{
	if (begin < end)
	{
		int middle = (begin + end) / 2, i, j, k, len = end - begin + 1;
		TRIANGLE* help = malloc(len * sizeof(TRIANGLE));

		mergeSort(array, begin, middle);
		mergeSort(array, middle + 1, end);

		i = begin, j = middle + 1, k = 0;
		while (i <= middle && j <= end)
			help[k++] = (array[i].area >= array[j].area) ? array[i++] : array[j++];

		while (i <= middle) help[k++] = array[i++];
		while (j <= end) help[k++] = array[j++];

		for (i = 0; i < len; i++)
			array[begin + i] = help[i];

		free(help);
	}
}

int Triangle_InequalityTheoremCheck_AndCircumference(TRIANGLE* polygon)
{
	double sideA = sqrt(power(polygon->p1.x - polygon->p2.x) + power(polygon->p1.y - polygon->p2.y)),
		sideB = sqrt(power(polygon->p2.x - polygon->p3.x) + power(polygon->p2.y - polygon->p3.y)),
		sideC = sqrt(power(polygon->p1.x - polygon->p3.x) + power(polygon->p1.y - polygon->p3.y));

	return (check(sideA + sideB, sideC)) ? 0 : ((check(sideA + sideC, sideB)) ? 0 :
		(check(sideB + sideC, sideA)) ? 0 : (polygon->circumference = sideA + sideB + sideC, 1));
	/* if it is possible to form a triangle, calculate circumference and return 1, otherwise return 0 */
}

int main(int argc, char* argv[]) {
	if (argc < 2)
		return printf("Too few arguments supplied!\n"), 1;

	FILE* file;
	if (!(file = fopen(argv[1], "r")))
		return printf("There was an error opening the file '%s'", argv[1]), 1;

	int len = 10, i = 0;
	TRIANGLE* triangle = malloc(len * sizeof(TRIANGLE));

	while (fscanf(file, "(%d,%d) (%d,%d) (%d,%d)\n", &triangle[i].p1.x, &triangle[i].p1.y, &triangle[i].p2.x,
		&triangle[i].p2.y, &triangle[i].p3.x, &triangle[i].p3.y) != EOF)
	{
		if (i == len)
			triangle = realloc(triangle, (len *= 2) * sizeof(TRIANGLE));

		if (Triangle_InequalityTheoremCheck_AndCircumference(triangle + i))
		{
			if (triangle[i].circumference > maxCircumference.circumference) maxCircumference = triangle[i];

			triangle[i++].area = fabs((triangle[i].p1.x*(triangle[i].p2.y - triangle[i].p3.y) +
				triangle[i].p2.x*(triangle[i].p3.y - triangle[i].p1.y) + triangle[i].p3.x*(triangle[i].p1.y - triangle[i].p2.y)) / 2);
		}

	}

	if (fclose(file))
		printf("File was unsuccessfully closed.\n");

	if (ferror(file))
		return printf("There was an error reading data from a file '%s'.\n", *(argv + 1)), clearerr(file), 1;
	else if (i < len)
		triangle = realloc(triangle, (i + 1) * sizeof(TRIANGLE));

	mergeSort(triangle, 0, i - 1); /* sort an array non-descending based on surfaces */

	if (!(file = fopen("MEASURE.txt", "w")))
		return printf("There was an error opening the file 'MEASURE.txt'\n"), 1;

	fprintf(file, "Biggest surface: %.2f | A=(%d,%d), B=(%d,%d), C=(%d,%d) |\n"
		"Biggest circumference: %.2f | A=(%d,%d), B=(%d,%d), C=(%d,%d) |\n",
		triangle->area, triangle->p1.x, triangle->p1.y, triangle->p2.x, triangle->p2.y, triangle->p3.x, triangle->p3.y,
		maxCircumference.circumference, maxCircumference.p1.x, maxCircumference.p1.y, maxCircumference.p2.x, maxCircumference.p2.y,
		maxCircumference.p3.x, maxCircumference.p3.y);

	if (ferror(file))
		return printf("There was an error reading data from a file 'MEASURE.txt'\n"), clearerr(file), 1;
	else
		printf("File 'MEASURE.txt' was successfully created.\n");

	if (fclose(file))
		printf("File was unsuccessfully closed.\n");

	free(triangle);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */