#pragma once
#include <stdio.h>
#include <stdlib.h>

typedef void* type;
typedef int bool;
#define true 1
#define false 0

typedef struct dll
{
	type key;
	struct dll* next;
	struct dll* prev;
}DLL;

DLL* createNode(type X);
bool addHead(DLL** phead, DLL** ptail, type data);
bool addTail(DLL** phead, DLL** ptail, type data);
void freeList(DLL** phead, DLL** ptail);
void freeListFromTail(DLL** phead, DLL** ptail);
bool deleteNode(DLL** phead, DLL** ptail, DLL* node);
bool insertAfter(DLL* node, type data);
bool insertBefore(DLL* node, type data);
DLL* searchSortedDLL(DLL* head, DLL* tail, type data);
void addSort(DLL** phead, DLL** ptail, type data);
int countNodes(DLL* head, DLL* tail);
DLL* getNthElement(DLL* head, DLL* tail, int n);
void swap(type a, type b);
DLL* partition(DLL* head, DLL* tail);
void quickSort(DLL* head, DLL* tail);
void print(DLL* head);

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */