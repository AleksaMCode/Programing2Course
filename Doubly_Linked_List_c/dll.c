#include "dll.h"

DLL* createNode(type X)
{
	DLL* newNode = malloc(sizeof(DLL));
	newNode->key = X;
	newNode->next = newNode->prev = NULL;
	return newNode;
}

bool addHead(DLL** phead, DLL** ptail, type data)
{
	DLL* newNode = createNode(data);
	if (!newNode)
		return false;
	if (*phead == NULL)
		*phead = *ptail = newNode;
	else
	{
		(*phead)->prev = newNode;
		newNode->next = *phead;
		*phead = newNode;
	}
	return true;
}

bool addTail(DLL** phead, DLL** ptail, type data)
{
	DLL* newNode = createNode(data);
	if (!newNode)
		return false;
	if (*phead == NULL)
		*phead = *ptail = newNode;
	else
	{
		(*ptail)->next = newNode;
		newNode->prev = *ptail;
		*ptail = newNode;
	}
	return true;
}

void freeList(DLL** phead, DLL** ptail)
{
	while (*phead)
	{
		DLL* tmp = (*phead)->next;
		free(*phead);
		*phead = tmp;
	}
	*phead = *ptail = NULL;
}

void freeListFromTail(DLL** phead, DLL** ptail)
{
	while (*ptail)
	{
		DLL* tmp = (*ptail)->prev;
		free(*ptail);
		*ptail = tmp;
	}
	*phead = *ptail = NULL;
}

bool deleteNode(DLL** phead, DLL** ptail, DLL* node)
{
	if (*phead == NULL || node == NULL)
		return false;
	if (node == *phead && node == *ptail) *phead = *ptail = NULL;
	else if (node == *phead) *phead = (*phead)->next, (*phead)->prev = NULL;
	else if (node == *ptail) *ptail = (*ptail)->prev, (*ptail)->next = NULL;
	else
	{
		node->prev->next = node->next;
		node->next->prev = node->prev;
	}
	free(node);
	return true;
}

bool insertAfter(DLL* node, type data)
{
	DLL* newNode = createNode(data);
	if (!newNode || !node || !(node->next))
		return false;
	newNode->next = node->next; node->next->prev = newNode;
	newNode->prev = node; node->next = newNode;
	return true;
}

bool insertBefore(DLL* node, type data)
{
	DLL* newNode = createNode(data);
	if (!newNode || !node || !(node->prev))
		return false;

	node->prev->next = newNode; newNode->next = node;
	newNode->prev = node->prev; node->prev = newNode;
	return true;
}

DLL* searchSortedDLL(DLL* head, DLL* tail, type data)
{
	if (!head)
		return NULL;

	while (head->key <data && tail->key > data)
		head = head->next, tail = tail->prev;

	return head->key == data ? head : tail->key == data ? tail : NULL;
}

void addSort(DLL** phead, DLL** ptail, type data)
{
	DLL* ph, *pt, *newNode = createNode(data);
	if (*phead == NULL) *phead = *ptail = newNode;
	else if ((*phead)->key > data) newNode->next = *phead, (*phead)->prev = newNode, *phead = newNode;
	else if ((*ptail)->key < data) newNode->prev = *ptail, (*ptail)->next = newNode, *ptail = newNode;
	else
	{
		for (ph = (*phead)->next, pt = (*ptail)->prev; ph->key <data && pt->key >data; ph = ph->next, pt = pt->next);
		if (pt->key < data) ph = pt;
		newNode->next = ph->next, newNode->prev = ph;
		ph->next->prev = newNode, ph->next = newNode;
	}
}

int countNodes(DLL* head, DLL* tail)
{
	int count = 0;
	while (head != tail || (head->prev != tail && tail->next != head))
		count+=2, head = head->next, tail = tail->prev;
	
	return head != tail ? count: ++count;
}

DLL* getNthElement(DLL* head, DLL* tail, int n)
{
	int count = countNodes(head, tail);
	if (count / 2 > n)
	{
		while (--n)
			head = head->next;
		return head;
	}
	else
	{
		while (--n)
			tail = tail->prev;
		return tail;
	}
}

void swap(type a, type b)
{
	/*<type>*/ tmp = *a;
	*a = *b;
	*b = tmp;
}

DLL* partition(DLL* head, DLL* tail)
{
	/* set pivot as tail of dll */
	type pivot = tail->key;
	DLL* i = head->prev;

	for (DLL* j = head; j != tail; j = j->next)
		if (j->key <= pivot)
		{
			i = (i == NULL) ? head : i->next;
			swap(i->key, j->key);
		}
	
	i = (i == NULL) ? head : i->next; 
	swap(i->key, tail->key);
	return i;
}

void quickSort(DLL * head, DLL * tail)
{
	if (tail != NULL && head != tail && head != tail->next)
	{
		DLL* p = partition(head, tail);
		quickSort(head, tail->prev);
		quickSort(head->next, tail);
	}
}

void print(DLL* head)
{
	while (head)
		printf("%<type> ", head->key), head = head->next;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */