/* Compute the sum by multiplying each digit in the sequence by its place value, and summing the results. */
#include <stdio.h>
#include <math.h>

int sum(unsigned num, unsigned digit_count)
{
	return digit_count == 1 ? num : digit_count * (int)(num / pow(10, digit_count - 1))
		+ sum(num - ((unsigned)(num / pow(10, digit_count - 1))) * (unsigned)pow(10, digit_count - 1), digit_count - 1);
}

int main()
{
	unsigned number;
	printf("Enter a number: "), scanf("%u", &number);
	printf("Sum = %d\n", sum(number, (unsigned)(floor(log10(number)) + 1)));
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */