#include "ConvertRGBToBMPBuffer.h"

BYTE* ConvertRGBToBMPBuffer(BYTE* Buffer, int width, int height, long* newsize)
{
	if ((NULL == Buffer) || (width == 0) || (height == 0))
		return NULL;

	int padding = 0;
	int scanlinebytes = width * 3;
	while ((scanlinebytes + padding) % 4 != 0)
		padding++;

	int psw = scanlinebytes + padding;

	*newsize = height * psw;

	BYTE* newbuf = calloc(*newsize, sizeof(BYTE));

	memset(newbuf, 0, *newsize);

	long bufpos = 0;
	long newpos = 0;
	for (int y = 0; y < height; y++)
		for (int x = 0; x < 3 * width; x += 3)
		{
			bufpos = y * 3 * width + x;
			newpos = (height - y - 1) * psw + x;

			newbuf[newpos] = Buffer[bufpos + 2];
			newbuf[newpos + 1] = Buffer[bufpos + 1];
			newbuf[newpos + 2] = Buffer[bufpos];
		}

	return newbuf;
}
