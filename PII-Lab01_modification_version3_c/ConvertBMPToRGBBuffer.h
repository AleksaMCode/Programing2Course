#ifndef CONVERTBMPTORGBBUFFER_H
#define CONVERTBMPTORGBBUFFER_H
#include <windows.h>
#include <stdlib.h>

BYTE* ConvertBMPToRGBBuffer(BYTE*, int, int);

#endif // !CONVERTBMPTORGBBUFFER_H
