#include "ReadAndProcessImage.h"
#include "ConvertBMPToRGBBuffer.h"
#include "ConvertRGBToBMPBuffer.h"

void ReadAndProcessImage(LPCTSTR input, LPCTSTR output)
{
	int x, y;
	long s, s2;
	BYTE* a = LoadBMP(&x, &y, &s, input);
	BYTE* b = ConvertBMPToRGBBuffer(a, x, y);
	InvertImage(b, x*y * 3);
	BYTE* c = ConvertRGBToBMPBuffer(b, x, y, &s2);
	SaveBMP(c, x, y, s2, output);
	free(a);
	free(b);
	free(c);
}