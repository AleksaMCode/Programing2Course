#include "InvertImage.h"
#include "LoadBMP.h"
#include "ReadAndProcessImage.h"
#include "SaveBMP.h"

/* 3rd version of lab modification */

void main()
{
	ReadAndProcessImage("test.bmp", "output.bmp");
}
	
/* mailto:aleksa */
/* Copyright(C) 2017 Original code modularized by: Aleksa Majkic */