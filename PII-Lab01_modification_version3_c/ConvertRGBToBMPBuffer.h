#ifndef CONVERTRGBTOBMPBUFFER_H
#define CONVERTRGBTOBMPBUFFER_H
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>    

BYTE* ConvertRGBToBMPBuffer(BYTE*, int, int, long*);

#endif // !CONVERTRGBTOBMPBUFFER_H
