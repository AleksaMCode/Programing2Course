#include "InvertImage.h"

void InvertImage(BYTE* image, int n)
{
	for (int i = 0; i < n; ++i)
		image[i] = 255 - image[i];
}