#include "LoadBMP.h"

BYTE* LoadBMP(int* width, int* height, long* size, const char* bmpfile)
{
	BITMAPFILEHEADER bmpheader = { 0 };
	BITMAPINFOHEADER bmpinfo = { 0 };
	FILE* file = fopen(bmpfile, "r");
	if (!file)
	{
		printf("Unable to open file!");
		return NULL;
	}

	fread(&bmpheader, sizeof(BITMAPFILEHEADER), 1, file);
	fread(&bmpinfo, sizeof(BITMAPINFOHEADER), 1, file);

	if (bmpheader.bfType != 'MB')
	{
		CloseHandle(file);
		return NULL;
	}

	*width = bmpinfo.biWidth;
	*height = abs(bmpinfo.biHeight);

	if (bmpinfo.biCompression != BI_RGB)
	{
		CloseHandle(file);
		return NULL;
	}

	if (bmpinfo.biBitCount != 24)
	{
		CloseHandle(file);
		return NULL;
	}


	*size = bmpheader.bfSize - bmpheader.bfOffBits;
	BYTE* Buffer = calloc(*size, sizeof(BYTE));
	fseek(file, bmpheader.bfOffBits, 0);
	fread(Buffer, sizeof(BYTE), *size, file);

	fclose(file);

	return Buffer;
}