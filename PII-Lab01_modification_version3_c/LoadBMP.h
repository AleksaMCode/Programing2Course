#ifndef LOADBMP_H
#define LOADBMP_H
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>

BYTE* LoadBMP(int*, int*, long*, const char*);

#endif // !LOADBMP_H
