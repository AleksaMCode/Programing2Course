#ifndef SAVEBMP_H
#define SAVEBMP_H
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>

#define bool int
#define true TRUE
#define false FALSE

bool SaveBMP(BYTE*, int, int, long, LPCTSTR);

#endif // !SAVEBMP_H
