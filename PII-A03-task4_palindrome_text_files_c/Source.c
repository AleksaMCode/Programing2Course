/* Write a program that opens a text file, whose name is entered as the first command-line argument.
Find and print all strings that are palindromes. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 100

int check_palindrome(const char* string) {
	char rev[MAX];
	strcpy(rev, string);
	_strrev(rev);

	return !strcmp(string, rev) ? 1 : 0;
}

int main(int argc, char* argv[]) {
	if (argc < 2)
		return printf("Too few arguments supplied!\n"), 1;

	FILE* input;
	if (!(input = fopen(argv[1], "r")))
		return printf("There was an error opening the file '%s'", argv[1]), 1;

	char string[MAX];
	printf("Palindromes are:\n");

	while (fscanf(input, "%s\n", string) != EOF)
		if (check_palindrome(string)) printf("%s\n", string);

	if (ferror(input))
		return printf("There was an error reading data from a file '%s'.\n", *(argv + 1)), clearerr(input), 1;

	if (fclose(input))
		printf("File was unsuccessfully closed.\n");

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */