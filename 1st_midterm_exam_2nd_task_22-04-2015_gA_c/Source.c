/* 1st midterm exam - 2nd task - 22.04.2015. - GROUP A */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct { char id[14], lname[20], fname[20], title[19]; float salary; }EMPLOYEES;

void insertion_sort(EMPLOYEES* array, int n)
{
	int j;
	for (int i = 1; i < n; i++)
	{
		EMPLOYEES x = array[i];
		for (j = i; j > 0 && (compare(x.id, array[j - 1].id) != 0); j--)
			array[j] = array[j - 1];
		array[j] = x;
	}
}

int compare(char first[], char second[])
{
	for (unsigned i = 0; i < strlen(first); i++)
	{
		if (first[i] < second[i]) return 1;
		else if (first[i] > second[i]) return 0;
	}
}

int seq_search_guard(EMPLOYEES array[], char key[])
{
	int i = 0;
	while (strstr(array[i].id, key) == NULL)
		i++;
	return i;
}

void sortit(EMPLOYEES* array, int n, FILE* prof, FILE* assis)
{
	fprintf(prof, "==== ============= ================= ================= ================= ========\n");
	fprintf(prof, "NUM. ID            L.NAME            F.NAME            TITLE             SALARY\n");
	fprintf(assis, "==== ============ ================= ================= ================= ========\n");
	fprintf(assis, "NUM. ID           L.NAME            F.NAME            TITLE             SALARY\n");
	for (unsigned i = 0, a = 0, p = 0; i < n; i++)
	{
		if (!strcmp("assistant", array[i].title) || !strcmp("senior_assistant", array[i].title))
			fprintf(assis, "%4d. ", ++a),
			fprintf(assis, "%-13s %-17s %-17s %-17s %8.2f\n", array[i].id, array[i].lname, array[i].fname, array[i].title, array[i].salary);
		else
			fprintf(prof, "%4d. ", ++p),
			fprintf(prof, "%-13s %-17s %-17s %-17s %8.2f\n", array[i].id, array[i].lname, array[i].fname, array[i].title, array[i].salary);
	}
	fprintf(prof, "==== ============= ================= ================= ================= ========\n");
	fprintf(assis, "==== ============ ================= ================= ================= ========\n");
}

int main()
{
	FILE *file, *output_prof, *output_assis;
	EMPLOYEES* array, employee;
	int len = 10;
	array = (EMPLOYEES*)malloc(len * sizeof(EMPLOYEES));

	if (!(file = fopen("EMPLOYEES.DAT", "rb")))
		return printf("There was an error opening the file 'EMPLOYEES.DAT'.\n"), 1;

	int n = 0;

	while (fread(&employee, sizeof(EMPLOYEES), 1, file))
	{
		if (n == len)
			array = realloc(array, (len *= 2) * sizeof(EMPLOYEES));
		array[n++] = employee;
	}

	if (fclose(file))
		printf("File was unsuccessfully closed.\n");

	{
		char id[14];
		do printf("Insert id: "), scanf("%s", id); while (strlen(id) < 13 || strlen(id) > 13);
		int check = 0;

		if ((check = seq_search_guard(array, id)) != n)
			printf("%s, %s (%s) - %.2f\n", array[check].lname, array[check].fname, array[check].title, array[check].salary);
	}

	insertion_sort(array, n);

	if (!(output_prof = fopen("PROFESSOR.txt", "w")))
		return printf("There was an error opening the file 'PROFESSOR.txt'.\n"), 1;
	if (!(output_assis = fopen("ASSISTANT.txt", "w")))
		return printf("There was an error opening the file 'ASSISTANT.txt'.\n"), 1;

	sortit(array, n, output_prof, output_assis);

	if (fclose(output_prof))
		printf("File was unsuccessfully closed.\n");
	if (fclose(output_assis))
		printf("File was unsuccessfully closed.\n");

	free(array);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2016 Aleksa Majkic */