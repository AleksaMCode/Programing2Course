/* Write a program (using Stack) that opens a text file, whose name is entered as the first command-line argument.
Find and print all strings that are palindromes. */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX 100

typedef struct stack { char info; struct stack* next; }STACK;
STACK* tos = NULL;

void push(char letter)
{
	STACK* new = malloc(sizeof(STACK));
	new->info = letter;
	new->next = tos;
	tos = new;
}

int pop(char* letter)
{
	if (!tos) return 0;
	*letter = tos->info;
	STACK* p = tos;
	tos = p->next;
	free(p);
	return 1;
}

void freeStack()
{
	if (!tos) return;
	char c;
	while (pop(&c));
}

int checkPalindrome(const char* string)
{
	for (unsigned i = 0; i < strlen(string); i++)
		push(string[i]);
	
	char letter;
	int i = 0;

	while (pop(&letter))
		if (letter != *(string + i++))
			return 0;

	return 1;
}

int main(int argc, char* argv[]) {
	if (argc < 2)
		return printf("Too few arguments supplied!\n"), 1;

	FILE* input;
	if (!(input = fopen(argv[1], "r")))
		return printf("There was an error opening the file '%s'", argv[1]), 1;

	char string[MAX];
	printf("Palindromes are:\n");

	while (fscanf(input, "%s\n", string) != EOF)
	{
		freeStack();
		if (checkPalindrome(string)) 
			printf("%s\n", string);
	}
	freeStack();

	if (ferror(input))
		return printf("There was an error reading data from a file '%s'.\n", *(argv + 1)), clearerr(input), 1;

	if (fclose(input))
		printf("File was unsuccessfully closed.\n");

	return 0;
}