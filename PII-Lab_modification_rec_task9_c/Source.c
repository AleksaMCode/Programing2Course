/* Find the largest ASCII value in a string. */
#include <stdio.h>
#include <math.h>
#include <string.h>

char maximum(char array[], int n)
{
	return n > 1 ? (int)(fmax(array[n - 1], maximum(array, n - 1))) : *array;
}

int main()
{
	char string[] = "{TestString}";

	printf("Largest ASCII value in a string '%s' has '%c'.\n", string, maximum(string, strlen(string)));
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */