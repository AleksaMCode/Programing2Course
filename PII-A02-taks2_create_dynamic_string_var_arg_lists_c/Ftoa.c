#include "Ftoa.h"

char* ftoa(double num)
{
#define DIGIT_COUNT 10
	char* buffer = calloc(DIGIT_COUNT, 1);
	_gcvt(num, DIGIT_COUNT, buffer);
	return buffer;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */