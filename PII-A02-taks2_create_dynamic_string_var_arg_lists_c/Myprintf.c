#include "Myprintf.h"
#include "Ftoa.h"

char* myprintf(char* format, ...)
{
	va_list args;
	va_start(args, format);
	char *string = calloc(1, 1);
#define INT_SIZE 10
	char val_int[INT_SIZE + 1], val_char, *val_str, *val_float, *val_double;
	char *pos[5];
	do
	{
		pos[0] = strstr(format, "%d");
		pos[1] = strstr(format, "%s");
		pos[2] = strstr(format, "%c");
		pos[3] = strstr(format, "%lf");
		pos[4] = strstr(format, "%f");
		char *min = pos[0];

		for (int i = 0; i < 5; i++)
		{
			if (min == NULL)
				min = pos[i];
			else if (pos[i] < min && pos[i] != 0)
				min = pos[i];
		}

		if (min == pos[0] && min)
		{
			_itoa(va_arg(args, int), val_int, 10);
			string = realloc(string, strlen(string) + pos[0] - format + strlen(val_int) + 1);
			strncat(string, format, pos[0] - format);
			strcat(string, val_int);
			format = pos[0] + 2;
		}
		else if (min == pos[1] && min)
		{
			val_str = va_arg(args, char *);
			string = realloc(string, strlen(string) + pos[1] - format + strlen(val_str) + 1);
			strncat(string, format, pos[1] - format);
			strcat(string, val_str);
			format = pos[1] + 2;
		}
		else if (min == pos[2] && min)
		{
			val_char = va_arg(args, char);
			char pom[2] = { 0 };
			pom[0] = val_char;
			string = realloc(string, strlen(string) + pos[2] - format + strlen(pom) + 1);
			strncat(string, format, pos[2] - format);
			strcat(string, pom);
			format = pos[2] + 2;
		}
		else if (min == pos[3] && min)
		{
			val_double = ftoa(va_arg(args, double));
			string = realloc(string, strlen(string) + pos[3] - format + strlen(val_double) + 1);
			strncat(string, format, pos[3] - format);
			strcat(string, val_double);
			format = pos[3] + strlen(val_double);
		}
		else if (min == pos[4] && min)
		{
			double a;
			a = (float)va_arg(args, double);
			val_float = ftoa(a);
			string = realloc(string, strlen(string) + pos[4] - format + strlen(val_float) + 1);
			strncat(string, format, pos[4] - format);
			strcat(string, val_float);
			format = pos[4] + 2;
		}

	} while (pos[0] || pos[1] || pos[2] || pos[3] || pos[4]);

	va_end(args);
	string = realloc(string, strlen(string) + strlen(format) + 1);
	strcat(string, format);

	return string;
}