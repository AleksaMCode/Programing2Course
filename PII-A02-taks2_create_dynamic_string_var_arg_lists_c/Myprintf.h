#ifndef MYPRINTF_H
#define MYPRINTF_H
#include <stdarg.h>
#include <string.h>

char* myprintf(char*, ...);

#endif // !MYPRINTF_H

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */