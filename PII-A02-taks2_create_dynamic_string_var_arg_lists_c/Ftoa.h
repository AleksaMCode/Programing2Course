#ifndef FTOA_H
#define FTOA_H
#include <stdlib.h>

char* ftoa(double);

#endif // !FTOA_H

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */