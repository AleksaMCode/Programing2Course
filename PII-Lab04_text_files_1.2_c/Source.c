#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

typedef struct { double x, y; }TrianglePoint;
typedef struct { TrianglePoint p1, p2, p3; double area; }TRIANGLE;

void mergeSort(TRIANGLE array[], int begin, int end)
{
	if (begin < end)
	{
		int middle = (begin + end) / 2, i, j, k, len = end - begin + 1;
		TRIANGLE* help = malloc(len * sizeof(TRIANGLE));

		mergeSort(array, begin, middle);
		mergeSort(array, middle + 1, end);

		i = begin, j = middle + 1, k = 0;
		while (i <= middle && j <= end)
			help[k++] = (array[i].area >= array[j].area) ? array[i++] : array[j++];

		while (i <= middle) help[k++] = array[i++];
		while (j <= end) help[k++] = array[j++];

		for (i = 0; i < len; i++)
			array[begin + i] = help[i];

		free(help);
	}
}

int main(int argc, char* argv[])
{
	if (argc < 2)
		return printf("Too few arguments supplied!\n"), 1;

	FILE* file;
	if (!(file = fopen(argv[1], "r")))
		return printf("There was an error opening the file '%s'", argv[1]), 1;

	int len = 10, i = 0;
	TRIANGLE* triangle = malloc(len * sizeof(TRIANGLE));

	while (fscanf(file, "(%lf,%lf) (%lf,%lf) (%lf,%lf)\n", &triangle[i].p1.x, &triangle[i].p1.y, &triangle[i].p2.x,
		&triangle[i].p2.y, &triangle[i].p3.x, &triangle[i].p3.y) != EOF)
	{
		if (i == len)
			triangle = realloc(triangle, (len *= 2) * sizeof(TRIANGLE));

		triangle[i++].area = fabs((triangle[i].p1.x*(triangle[i].p2.y - triangle[i].p3.y)
			+ triangle[i].p2.x*(triangle[i].p3.y - triangle[i].p1.y) + triangle[i].p3.x*(triangle[i].p1.y - triangle[i].p2.y)) / 2);
	}

	if (fclose(file))
		printf("File was unsuccessfully closed.\n");

	if (ferror(file))
		return printf("There was an error reading data from a file '%s'.\n", *(argv + 1)), clearerr(file), 1;
	else if (i < len)
		triangle = realloc(triangle, (i + 1) * sizeof(TRIANGLE));

	mergeSort(triangle, 0, i - 1); /* sort an array non-descending based on surfaces */

	printf("Triangle with the biggest surface (S=%.2f) is:\n(%.2lf,%.2lf) (%.2lf,%.2lf) (%.2lf,%.2lf)\n",
		triangle->area, triangle->p1.x, triangle->p1.y, triangle->p2.x, triangle->p2.y, triangle->p3.x, triangle->p3.y);

	free(triangle);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */