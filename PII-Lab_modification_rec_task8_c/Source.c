/* Remove a digit from a number using recursion. */
/* PROBLEM WITH : e.q. 1511620151 (that contains number 0 as a digit(s)); works fine with other numbers e.q. 54841511 */ 
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
typedef int bool;

unsigned eject_function(unsigned, bool(*)(unsigned));
unsigned eject(unsigned, unsigned);
bool even(unsigned);
bool odd(unsigned);

int main()
{
	unsigned n;
	printf("Enter a number: "), scanf("%u", &n);
	printf("Eject all even digits: %u\n", eject_function(n, even));
	printf("Eject all odd digits: %u\n", eject_function(n, odd));
	return 0;
}

unsigned eject(unsigned number, unsigned digit)
{
	static i;
	if (!(number % 10))
		return i = 0, number;
	if (number % 10u == digit)
		i--, number -= number % 10;

	return (number % 10u) * (unsigned)(pow(10, i++)) + eject(number / 10, digit);
}


/* puts 0 in place of a digit
unsigned eject(unsigned number, unsigned digit)
{
	static i;
	if (!(number % 10))
		return i--, number;
	if (number % 10u == digit)
		number -= number % 10;

	return (number % 10u) * (unsigned)(pow(10, i++)) + eject(number / 10, digit);
}
*/

bool even(unsigned num)
{
	return !odd(num);
}

bool odd(unsigned num)
{
	return num & 1;
}

unsigned eject_function(unsigned number, bool(*pf)(unsigned))
{
	for (unsigned i = 1u; i <= 9u; i++)
		if ((*pf)(i))
		{
			char cnum[11] = { 0 };
			_itoa(number, cnum, 10);

			if (strchr(cnum, i + 48))
				number = eject(number, i);
		}
	return number;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */