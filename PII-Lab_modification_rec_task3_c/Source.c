/* Reverse a number using recursion. */
#include <stdio.h>
#include <math.h>

void reverse_number(unsigned n, unsigned* result)
{
	static reversed;

	reversed += n % 10 * (int)pow(10, floor(log10(n)));
	*result = reversed;
	if (n / 10)
		reverse_number(n / 10, result);
}

int reverse_number2(int n)
{
	return n < 10 ? n : (n % 10) * (unsigned)pow(10, floor(log10(n))) + reverse_number2(n / 10);
}

int main()
{
	unsigned n, revers;
	do printf("Enter a number: "), scanf("%u", &n); while (n == 0);
	reverse_number(n, &revers);
	printf("%u -> %u\n", n, revers);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2016 Aleksa Majkic */