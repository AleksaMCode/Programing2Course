/* Find the largest number in the array. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int maximum(int array[], int n)
{
	return n > 1 ? (int)(fmax(array[n-1], maximum(array, n - 1))) : *array;
}

int main()
{
	int n;
	printf("Enter size of an array? "), scanf("%d", &n);
	int* array = malloc(n * 4);
	for (int i = 0; i < n; i++)
		printf("%d. number: ", i + 1), scanf("%d", &array[i]);

	printf("Largest number in a array is %d.\n", maximum(array, n));

	free(array);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */