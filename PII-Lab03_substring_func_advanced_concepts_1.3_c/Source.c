/* Write a program that checks whether the string 'substring' is located in a string 'string' at least n times.*/
/* e.g. D:\>program.exe -p ao -s "ao eoaoaoelf" -n 2 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	char* string = NULL, *substring = NULL;
	int n = 1, total = 0;

	for (int i = 0; i < argc; i++)
	{
		if (!strcmp(argv[i], "-p"))
			substring = argv[++i];
		if (!strcmp(argv[i], "-s"))
			string = argv[++i];
		if (!strcmp(argv[i], "-n"))
			n = atoi(argv[++i]);
	}

	if (argc >= 7)
		if (strcmp(argv[1], "-p") && strcmp(argv[1], "-s") && strcmp(argv[1], "-n"))
			return printf("Error. Unknown option '%s'.\n", argv[2]), 1;
		else if (strcmp(argv[3], "-p") && strcmp(argv[3], "-s") && strcmp(argv[3], "-n"))
			return printf("Error. Unknown option '%s'.\n", argv[4]), 1;
		else if (strcmp(argv[5], "-p") && strcmp(argv[5], "-s") && strcmp(argv[5], "-n"))
			return printf("Error. Unknown option '%s'.\n", argv[5]), 1;

	if (!string)
		return printf("Error. String s is missing.\n"), 1;
	else if (!substring)
		return printf("Error. String p is missing.\n"), 1;

	char* cpystring = string;
	if (strlen(substring) <= strlen(string))
		while (string = strstr(string, substring))
			total++, string++;

	printf("String '%s' %s located in a string '%s' at least %d time(s).\n", substring, (total == n) ? "is" : "isn't", cpystring, n);

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */