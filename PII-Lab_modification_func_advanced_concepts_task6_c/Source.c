/* Find max. positiv and even subarray of numbers using a function void max_subarray(int*, int, int(*)(int), int**, int*); 
   + find max. prime subarray */
#include <stdio.h>
#include <stdlib.h>

void max_subarray(int*, int, int(*)(int), int**, int*);
int prime(int);
int positiv(int);
int even(int);
void print_array(int*, int*, int);

int main()
{
	int* beginning = (void*)0, length = 0, n;
	do printf("Size of an array is: "), scanf("%d", &n); while (n <= 0);
	int* array = malloc(n * 4);
	for (int i = 0; i < n;)
		printf("array[%d]: ", i + 1), scanf("%d", array + i++);

	max_subarray(array, n, even, &beginning, &length);
	printf("Maximum even subarray (total %d element(s)):\n", length);
	if (beginning)
		print_array(array, beginning, length);
	else
		beginning = NULL, length = 0;

	max_subarray(array, n, &positiv, &beginning, &length);
	printf("Max. positiv subarray (total %d element(s)):\n", length);
	if (beginning)
		print_array(array, beginning, length);
	else
		beginning = NULL, length = 0;

	max_subarray(array, n, prime, &beginning, &length);
	printf("Maximum prime subarray (total %d element(s)):\n", length);
	if (beginning)
		print_array(array, beginning, length);
	else
		beginning = NULL, length = 0;


	free(array);
	return 0;
}

void max_subarray(int* array, int n, int(*pf)(int), int** beginning, int* length)
{
	int count = *length = 0;
	for (int i = 0; i < n; i++, count = 0)
	{
		for (int j = i; j < n && (*pf)(array[j]); j++)
			count++;

		if (count > *length)
			*length = count, *beginning = array + i;
	}
}

int positiv(int num)
{
	return n > 0;
}

int even(int num)
{
	return !num ? 0 : !(num & 1);
}

int prime(int num)
{
	if (num <= 2) 
		return 0;

	for (int i = 2;  i*i <= num; i++)
		if (!(num%i))
			return 0;
	return 1;
}

void print_array(int* arr, int* beginning, int length)
{
	for (int i = 0; i < length; i++)
		printf("%d ", *(beginning + i));

	printf("\n");
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */