#include <SLL.h>

int main(int argc, char *argv[])
{
	if (argc < 2)
		return printf("Too few arguments supplied!\n"), 1;

	FILE *input;
	SLL *list = NULL, *tmp, *first;

	if (!(input = fopen(argv[1], "r")))
		return printf("There was an error opening the file '%s'", argv[1]), 1;

	double next;
	if (fscanf(input, "%lf", &next))
	{
		first = insertBeginning(&list, next);
		
		while (fscanf(input, "%lf", &next) != EOF)
		{
			tmp = findElement(list, next);
			if (!tmp) 
				insertAfter(*(&first), next);
		}
	}
	
	if (fclose(input))
		printf("File was unsuccessfully closed.\n");

	selectionSort(list);

	printRecursive(list);

	getchar();
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */