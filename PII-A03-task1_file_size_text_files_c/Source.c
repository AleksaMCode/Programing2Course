/* Write a program that writes to a file, whose name is entered as a command line argument,
enter the first n natural numbers (n is set from standard input), and then calculates the size of the file in bytes. */

#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[]) {
	if (argc < 2)
		return printf("Too few arguments supplied!\n"), 1;

	FILE* output;
	if (!(output = fopen(argv[1], "w"))) 
		return printf("There was an error opening the file '%s'.\n", argv[1]), 1;

	int n;
	printf("Enter the upper limit [1...n]: "), scanf("%d", &n);

	for (int i = 1; i <= n; i++)
		if (fprintf(output, "%d", i) < 0) 
			printf("There was an error writing the number '%d' in a file '%s'.\n", i, argv[1]);

	printf("File size: %ld bytes\n", ftell(output));
	if (fclose(output))
		printf("File was unsuccessfully closed.\n");

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */