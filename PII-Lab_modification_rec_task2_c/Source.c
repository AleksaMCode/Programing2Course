/* Calculate prime factors of a number n using recursion. */ 
#include <stdio.h>
#include <math.h>

void prime_factors(int number)
{
	int divisor = 2;
	if (number == 1)
	{
		printf("1\n");
		return;
	}
	while ((number % divisor) && (number > divisor))
		divisor++;
	printf("%d*", divisor);
	prime_factors(number / divisor);
}

int main()
{
	int n;
	printf("Enter number: "), scanf("%d", &n);

	printf("%d=", n);
	prime_factors(n);

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */