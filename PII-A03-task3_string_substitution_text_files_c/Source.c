/* Write a program that opens a text file, whose name is entered as the first command-line argument.
Replaces all occurrences of the string referred to as the second command line argument
with the string referred to as the third command line argument. */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void transform(char* string, char* findS, char* replaceS, FILE* file) {
	char* s, *leftover, *help = calloc(strlen(string), 1);

	while (leftover = s = strstr(string, findS))
	{
		leftover += strlen(findS);
		strcpy(help, leftover);
		strcpy(s, replaceS);
		strcat(string, help);
	}

	for (unsigned i = 0; i < strlen(string); i++)
		if (fputc(string[i], file) == EOF)
			break;

	if (ferror(file))
		printf("There was an error writing data in a file!\n"), clearerr(file);

	free(help);
}

int main(int argc, char* argv[]) {
	if (argc < 4)
		return printf("Too few arguments supplied!\n"), 1;

	FILE* file;
	if (!(file = fopen(argv[1], "r")))
		return printf("There was an error opening the file '%s'", argv[1]), 1;

	int len = 50, i = 0;
	char* string = calloc(len, 1), symbol;
	while ((symbol = fgetc(file)) != EOF)
	{
		if (i == len - 1) 
			string = realloc(string, (len *= 2));

		string[i++] = symbol;
	}

	if (ferror(file))
		return printf("There was an error reading data from a file '%s'.\n", argv[1]), clearerr(file), 1;
	else if (i < len - 1)
		string = realloc(string, i + 1);
	string[i] = 0;

	file = freopen(argv[1], "w", stdout);
	transform(string, argv[2], argv[3], file);
	if (fclose(file)) 		
		printf("File was unsuccessfully closed.\n");

	free(string);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */