/* Extract decimal part from a floating point numbers using a function with function pointer as argument. */ 
#include <stdio.h>
#include "Form.h"

void print_array(const double* array, int n)
{
	printf("{ %.2lf, ", array[0]);
	for (int i = 1; i < n - 1; i++)
		printf("%.2lf, ", array[i]);
	printf("%.2lf }\n", array[n - 1]);
}

int main()
{
	int n;
	do printf("n= "), scanf("%d", &n); while (n <= 0);
	double* arr1 = malloc(n * 8),
		*arr2 = malloc(n * 8);

	for (int i = 0; i < n; i++)
		printf("array[%d]: ", i + 1), scanf("%lf", arr1 + i);

	form(arr1, arr2, n, get_decimal_part);
	print_array(arr1, n);
	print_array(arr2, n);

	free(arr1);
	free(arr2);

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */