#include "Form.h"

void form(double a[], double b[], int n, double(*f)(double))
{
	for (int i = 0; i < n; i++)
		b[i] = (*f)(a[i]);
}

double get_decimal_part(double number)
{
	return number - (int)number;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */