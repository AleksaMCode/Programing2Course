#ifndef FORM_H
#define FORM_H
#include <stdlib.h>

void form(double[], double[], int, double(*)(double));
double get_decimal_part(double);

#endif // !FORM_H

/* mailto:aleksa */ 
/* Copyright(C) 2017 Aleksa Majkic */