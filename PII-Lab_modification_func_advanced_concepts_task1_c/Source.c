/* Add option: my_printf("%b", 5u); */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

void my_printf(char* format, ...)
{
	va_list args;
	va_start(args, format);
	for (int i = 0; format[i]; i++)
	{
		if (format[i] == '%' && format[i + 1] == 'b')
		{
			char buffer[33];
			_itoa((int)va_arg(args, unsigned), buffer, 2);
			printf("%s", buffer);
			i++;
		}
		else if (format[i] == '%' && format[i + 1] == 'd')
		{
			printf("%d", va_arg(args, int));
			i++;
		}
		else if (format[i] == '%' && format[i + 1] == 'c')
		{
			printf("%c", va_arg(args, char));
			i++;
		}
		else if (format[i] == '%' && format[i + 1] == 's')
		{
			printf("%s", va_arg(args, char*));
			i++;
		}
		else if (format[i] == '%' && format[i + 1] == 'f')
		{
			printf("%f", va_arg(args, float));
			i++;
		}
		else if (format[i] == '%' && format[i + 1] == 'l' && format[i + 2] == 'f')
		{
			printf("%lf", va_arg(args, double));
			i += 2;
		}
		else
		{
			printf("%c", format[i]);
		}
	}
	va_end(args);
}


int main()
{
	my_printf("%b", 5u);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */