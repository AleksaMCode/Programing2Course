/* Using recursion determine whether a word is a palindrome. */
#include <stdio.h>
#include <string.h>

int is_palindrome(char s[], int n)
{
	static i = 0;
	if (strlen(s) == 1) return 0;
	if (s[0] != s[n - i++ - 1]) return 1;
	else return is_palindrome(s + 1, n - 1);
}

int main()
{
	char string[50] = { 0 };
	printf("Enter a word: "), scanf("%s", string);
	printf("Word '%s' %s a palindrome.\n", string, !is_palindrome(string, (int)strlen(string)) ? "is" : "is not");

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2016 Aleksa Majkic */