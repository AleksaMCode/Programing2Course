/* Find max. positiv and even subarray of numbers using a function void max_subarray(int*, int, int(*)(int), int**, int*);
+ find max. subarray of palindrome numbers */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void max_subarray(int*, int, int(*)(int), int**, int*);
int palindrome(int);
int positiv(int);
int even(int);
void print_array(int*, int*, int);

int main()
{
	int* beginning = (void*)0, length = 0, n;
	do printf("Size of an array is: "), scanf("%d", &n); while (n <= 0);
	int* array = malloc(n * 4);
	for (int i = 0; i < n;)
		printf("array[%d]: ", i + 1), scanf("%d", array + i++);

	max_subarray(array, n, even, &beginning, &length);
	printf("Maximum even subarray (total %d element(s)):\n", length);
	if (beginning)
		print_array(array, beginning, length);
	else
		beginning = NULL, length = 0;

	max_subarray(array, n, &positiv, &beginning, &length);
	printf("Max. positiv subarray (total %d element(s)):\n", length);
	if (beginning)
		print_array(array, beginning, length);
	else
		beginning = NULL, length = 0;

	max_subarray(array, n, palindrome, &beginning, &length);
	printf("Maximum palindrome subarray (total %d element(s)):\n", length);
	if (beginning)
		print_array(array, beginning, length);
	else
		beginning = NULL, length = 0;


	free(array);
	return 0;
}

void max_subarray(int* array, int n, int(*pf)(int), int** beginning, int* length)
{
	int count = *length = 0;
	for (int i = 0; i < n; i++, count = 0)
	{
		for (int j = i; j < n && (*pf)(array[j]); j++)
			count++;

		if (count > *length)
			*length = count, *beginning = array + i;
	}
}

int positiv(int num)
{
	return num > 0;
}

int even(int num)
{
	return !num ? 0 : !(num & 1);
}

int palindrome(int num)
{
	if ((int)log10((double)num) + 1 < 2)
		return 0;

	int n = num;
	int rev = 0;
	while (num > 0)
	{
		int dig = num % 10;
		rev = rev * 10 + dig;
		num = num / 10;
	}

	return n == rev ? 1 : 0;
}

void print_array(int* arr, int* beginning, int length)
{
	for (int i = 0; i < length; i++)
		printf("%d ", *(beginning + i));

	printf("\n");
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */