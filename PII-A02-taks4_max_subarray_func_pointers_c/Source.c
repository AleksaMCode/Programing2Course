/* Find max. positiv and even subarray of numbers using a function with function pointer as argument. */
#include "MaxSubarray.h"

int main()
{
	int array[] = { -1, 2, 4, 6, -8, 3, 7, 9, 2, 1, 9, 11, 13, 15, 7 };
	print_array(array, array + sizeof(array) / 4);
	printf("\nMaximum positiv subarray: ");
	max_subarray(array, sizeof(array) / 4, positiv);
	printf("\nMaximum even subarray: ");
	max_subarray(array, sizeof(array) / 4, &even);

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */