#include "MaxSubarray.h"

void max_subarray(int* array, size_t length, bool(*pparameter)(int))
{
	int max = 0, count = max, *beginning = (void*)0;
	for (unsigned i = 0; i < length; i++, count = 0)
	{
		for (unsigned j = i; j < length && (*pparameter)(array[j]); j++)
			count++;

		if (count > max)
			max = count, beginning = array + i;
	}

	print_array(beginning, beginning + max);
}

/* check if number is positive */
bool positiv(int num)
{
	return num > 0;
}

/* check if number is even */
bool even(int num)
{
	return !num ? 0 : !(num & 1);
}

void print_array(int* ptr, const int* end)
{
	for (; ptr != end;)
		printf("%d ", *(ptr++));
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */