#ifndef MAXSUBARRAY_H
#define MAXSUBARRAY_H
#include <stdio.h>
#include <stdlib.h>
#define bool int

void max_subarray(int*, size_t, bool(*)(int));
bool positiv(int);
bool even(int);
void print_array(int*, const int*);

#endif // !MAXSUBARRAY_H

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */