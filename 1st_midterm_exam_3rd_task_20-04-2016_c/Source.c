/* 1st midterm exam - 3rd task - 20.04.2016. - GROUP A */
#include <stdio.h>
#include <stdlib.h>

int main()
{
	FILE* input;
	if (!(input = fopen("FLOATS.DAT", "r+b")))
		return printf("There was an error opening the file 'FLOATS.DAT'.\n"), 1;

	fseek(input, 0, 2);
	int n = ftell(input) / 4;
	rewind(input);
	/* SHELL SORT */
	float first, second;
	int j;
	for (int h = n / 2; h > 0; h /= 2)
	{
		for (int i = h; i < n; i++)
		{
			fseek(input, i * 4, 0);
			fread(&first, 4, 1, input);
			for (j = i; fseek(input, (j - h) * 4, 0), fread(&second, 4, 1, input), j >= h && first < second; j -= h)
				fseek(input, j * 4, 0), fwrite(&second, 4, 1, input), fflush(input);

			fseek(input, j * 4, 0), fwrite(&first, 4, 1, input), fflush(input);
		}
	}

	if (fclose(input))
		printf("File was unsuccessfully closed.\n");

	/* Code to check if numbers are sorted
	float f;
	if (!(input = fopen("FLOATS.DAT", "rb"))) return printf("There was an error opening the file 'FLOATS.DAT'.\n"), 1;
	while (fread(&f, 4, 1, input))
	printf("%.2f ", f);
	printf("\n"), fclose(input); */
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2016 Aleksa Majkic */