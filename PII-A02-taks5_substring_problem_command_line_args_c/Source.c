/* Write a program that checks whether the string 'substring' is located in a string 'string' at least n times. 
Strings and the number n are used as command line arguments. */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char* argv[]) 
{
	if (argc < 3) return printf("Too few arguments supplied!\n"), 1; /* argc should be 3 for correct execution */
	char* string = argv[2], 
		*substring = argv[1];
	unsigned total = 0;

	if (strlen(substring) <= strlen(string))
		while (string = strstr(string, substring))
			total++, string++;

	printf("String '%s' %s located in a string '%s' %dx times.\n", substring, (total == atoi(argv[3])) ? "is" : "isn't", argv[2], atoi(argv[3]));

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */