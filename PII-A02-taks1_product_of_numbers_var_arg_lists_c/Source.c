/* Write a function with a variable number of arguments that accounts product of any number of real numbers. */
#include <stdio.h>
#include <stdarg.h>

double product(unsigned n, ...)
{
	double prod = 1;
	va_list arg;
	va_start(arg, n);

	for (int i = 1; i <= n; i++)
		prod *= va_arg(arg, double);

	va_end(arg);
	return prod;
}

int main()
{
	printf("10 * 20.5 * 4.6659 = %.2lf\n", product(3, 10.0, 20.5, 4.6659));
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */