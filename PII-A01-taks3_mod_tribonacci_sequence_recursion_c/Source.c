/* Write a recursive function to calculate modified Tribonacci sequence ( 1, 2, 3, 6, 11, ...) + optimize recursion using memoization. */
/* The Tribonacci sequence is a generalization of the Fibonaccisequence where each term is the sum of the three preceding terms. */
/* The sequence begins: 0, 1, 1, 2, 4, 7, 13 ... */
/* Modified Tribonacci sequence: 1, 2, 3, 6, 11, 20 ...*/
#include <stdio.h>
#define LIMIT 100

unsigned mod_tribonacci_sequence(unsigned n)
{
	return n == 1 ? 1 : n == 2 ? 2 : n == 3 ? 3 : mod_tribonacci_sequence(n - 1) + mod_tribonacci_sequence(n - 2) + mod_tribonacci_sequence(n - 3);
}

/* optimize recursion - memoization*/
unsigned mod_trib_seq_memo(unsigned n)
{
	static unsigned memo[LIMIT] = { 1, 2, 3 };
	if (!memo[n]) memo[n] = mod_trib_seq_memo(n - 1) + mod_trib_seq_memo(n - 2) + mod_trib_seq_memo(n - 3);

	return memo[n];
}

int main()
{
	unsigned n;
	do printf("Enter desired position in sequence: "), scanf("%u", &n); while (n == 0 || n > LIMIT);
	printf("%u. member of sequence is: %u\n", n, mod_tribonacci_sequence(n));
	printf("%u. member of sequence is: %u\n", n, mod_trib_seq_memo(n - 1));

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */