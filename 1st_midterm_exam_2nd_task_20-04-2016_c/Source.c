/* 1st midterm exam - 2nd task - 20.04.2016. - GROUP A */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct serb_engl { char serb[16], eng[16]; }SERB_ENGL;

int seq_search(SERB_ENGL* doubles, int n, char* word_serbian)
{
	for (int i = 0; i < n; i++)
		if (!(strcmp(doubles[i].serb, word_serbian)))
			return i;
	return -1;
}

int main(int argc, char* argv[])
{
	if (argc < 2)
		return printf("Too few arguments supplied!\n"), 1;

	FILE* input;
	struct serb_engl* array;
	SERB_ENGL next;
	int len = 10, n = 0, index;
	char word_s[16];
	array = malloc(len * sizeof(SERB_ENGL));

	if (!(input = fopen(argv[1], "r")))
		return printf("There was an error opening the file '%s'", argv[1]), 1; /* argv[1] = "DICTIONARY.txt" */

	while (fscanf(input, "%s %s\n", next.serb, next.eng) != EOF)
	{
		if (n == len)
			array = realloc(array, (len *= 2) * sizeof(SERB_ENGL));

		array[n++] = next;
	}

	if (fclose(input))
		printf("File was unsuccessfully closed.\n");

	do printf("Enter a word in serbian: "), scanf("%s", word_s); while (strlen(word_s) > 16);

	if ((index = seq_search(array, n, word_s)) != -1)
		printf("%s -> %s\n", array[index].serb, array[index].eng);

	free(array);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2016 Aleksa Majkic */