/* Write a program in which you, from a binary file whose name is the first command-line argument, 
read (binary entered, unsorted) information about an unknown number of students and establish appropriate dynamic array,
and then sort the set descending according to the average grade and formatte the print on standard output. 
Data kept on the student are: the index number, surname, name and average grade. */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct { char index[10], first_name[20], surname[20]; double average_grade; }STUDENT;

/* QUICKSORT
Best-case performance: O(n log n) (simple partition) or O(n) (three-way partition and equal keys)
Average performance: O(n log n)
Worst-case space complexity: O(n)*/

int cmp(const void *el_1, const void *el_2)
{
	return ((STUDENT*)el_1)->average_grade > (*(STUDENT*)el_2).average_grade;
}

int main(int argc, char* argv[])
{
	if (argc < 2)
		return printf("Too few arguments supplied!\n"), 1;

	FILE* input;
	int len = 10, n = 0;
	STUDENT* array = malloc(sizeof(STUDENT) * len), catch;


	if (!(input = fopen(argv[1], "rb")))
		return printf("There was an error opening the file '%s'", argv[1]), 1;

	while (fread(&catch, sizeof(STUDENT), 1, input))
	{
		if (n == (len + 1))
			array = realloc(array, (len *= 2) * sizeof(STUDENT));
		array[n++] = catch;
	}

	if (n < len)
		array = realloc(array, n * sizeof(STUDENT));

	/* void qsort(void *base, size_t nitems, size_t size, int (*compar)(const void *, const void*)); */
	qsort(array, n, sizeof(STUDENT), cmp);

	printf("==== ==================== ==================== ========== =====\n");
	printf("NUM. FIRST NAME           LAST NAME             N.INDEX   GRADE\n");
	printf("==== ==================== ==================== ========== =====\n");
	for (int i = 0; i < n; i++)
		printf("%3d. %-20s %-20s %-10s %5.2lf\n", i + 1, array[i].first_name, array[i].surname, array[i].index, array[i].average_grade);
	printf("==== ==================== ==================== ========== =====\n");

	if (fclose(input))
		printf("File was unsuccessfully closed.\n");

	free(array);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */