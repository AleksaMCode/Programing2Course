# Welcome
Collection of codes I wrote in C for course "Programing 2" (simplified version of course "Data Structures and Algorithms" | Functions advanced concepts | Text and binary files).

[-->DOWNLOAD WHOLE GIT REPOSITORY](https://gitlab.com/AleksaMCode/Programing2Course/repository/archive.zip?ref=master)

# Course "Programing 2" theory
You can download [oral exams solution here in pdf.](https://gitlab.com/AleksaMCode/Programing2Course/raw/master/oral_exam_v1.pdf)
#### [Theory written in Serbian](https://gitlab.com/AleksaMCode/Programing2Course/wikis/home)
* [Recursion](https://gitlab.com/AleksaMCode/Programing2Course/wikis/recursion-%7C-recursive-algorithms-%5Bserbian%5D)
* [Function with a variable number of arguments](https://gitlab.com/AleksaMCode/Programing2Course/wikis/function-with-a-variable-number-of-arguments-%5Bserbian%5D)
* [Function pointers](https://gitlab.com/AleksaMCode/Programing2Course/wikis/function-pointers-%5Bserbian%5D)
* [Command line arguments](https://gitlab.com/AleksaMCode/Programing2Course/wikis/command-line-arguments-%5Bserbian%5D)

# EXAMS
* [1. midterm exam - 20.04.2016. (1. task) - GROUP A](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/1st_midterm_exam_1st_task_20-04-2016_c/Source.c)
* [1. midterm exam - 20.04.2016. (2. task) - GROUP A](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/1st_midterm_exam_2nd_task_20-04-2016_c/Source.c)
 * [Text file - DICTIONARY.txt](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/1st_midterm_exam_2nd_task_20-04-2016_c/DICTIONARY.txt)
* [1. midterm exam - 20.04.2016. (3. task) - GROUP A (SHELL SORT)](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/1st_midterm_exam_3rd_task_20-04-2016_c/Source.c)
 * [Download binary file - FLOATS.DAT](https://gitlab.com/AleksaMCode/Programing2Course/raw/master/1st_midterm_exam_3rd_task_20-04-2016_c/FLOATS.DAT)
* [1. midterm exam - 22.04.2015. (1. task) - GROUP A](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/1st_midterm_exam_1st_task_22-04-2015_gA_c/Source.c)
 * Text files - [WORDS.txt](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/1st_midterm_exam_2nd_task_20-04-2016_c/DICTIONARY.txt) and [REVERSE.txt](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/1st_midterm_exam_1st_task_22-04-2015_gA_c/REVERSE.txt)
* [1. midterm exam - 22.04.2015. (2. task) - GROUP A (INSERTION SORT + SEQUENTIAL SEARCH)](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/1st_midterm_exam_2nd_task_22-04-2015_gA_c/Source.c)

# LABORATORY
#### LAB01 - Modular programming (Modularization)
* [3rd version of lab modification](https://gitlab.com/AleksaMCode/Programing2Course/tree/master/PII-Lab01_modification_version3_c) 

[Detailed explanation of the task in pdf.](https://gitlab.com/AleksaMCode/Programing2Course/raw/master/PII-Lab01_modification_version3_c/PII-Lab01-3.pdf)
#### LAB02 - Recursion
* 1.1 [Write a program that loads the (unsigned) number n, and then prints the sum of digits of n](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A01-taks1_sum_of_digits_recursion_c/Source.c)
* 1.2 [Compute the sum by multiplying each digit in the sequence by its place value, and summing the results](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab02_recursion_1.2_c/Source.c)
* 1.3 [Calculate Hamming weight (population count, popcount, or sideways sum) for one unsigned number](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab02_hamming_weight_recursion_1.3_c/HammingWeight.c)
* 1.1 MODIFICATION : [Calculate largest digit in a number](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab02_modification_recursion_1.1_c/Source.c)

#### LAB03 - Functions advanced concepts (Function with a variable number of arguments | Function pointers | Command line arguments)
* 1.1 [Create a function ``void my_printf(char* format, ...);``](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab03_myprintf_func_advanced_concepts_1.1_c/Source.c)
* 1.2 [Find max. positiv and even subarray of numbers using a function ``void max_subarray(int*, int, int(*)(int), int**, int*);``](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab03_max_subarray_func_advanced_concepts_1.2_c/Source.c)
* 1.3 [Write a program that checks whether the string p is located in a string s at least n times](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab03_substring_func_advanced_concepts_1.3_c/Source.c)

#### LAB04 - Text files
* [1.1](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab04_text_files_1.1_c/Source.c)
* [1.2](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab04_text_files_1.2_c/Source.c)

#### LAB05 - Binary files and sorting algorithms
* 1.1 [Write a program in which u need to read from a file whose name is the first command-line argument, read binary intered 4-byte data then print out first n numbers in ascending order, where n is referred to as the second argument in the command line (COMB SORT)](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab05_binary_files_and_sorting_algo_1.1_c/Source.c)
    * [Download binary file](https://gitlab.com/AleksaMCode/Programing2Course/raw/master/PII-Lab05_binary_files_and_sorting_algo_1.1_c/5num_4byte.bin)
* 1.2 [Write a program which must loade from standard input data for n students and form appropriate dynamic array, and then loaded set of data on students write in binary file whose named is first command line argument. Data kept on the student are: last name, first name and average grade](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab05_binary_files_and_sorting_algo_1.2_c/Source.c)
* 1.3 [Write a program in which you, from a binary file whose name is the first command-line argument, read (binary entered, unsorted) information about an unknown number of students and establish appropriate dynamic array, and then sort the set descending according to the average grade and formatte the print on standard output.<br />Data kept on the student is: last name, first name and average grade (C library function - QSORT)](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab05_binary_files_and_sorting_algo_1.3_c/Source.c)

#### LAB06 - Singly Linked Lists
* 1.1
    * [Source.c](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab06_singly_linked_list_task1_c/Source.c)
    * [SLL.h](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab06_singly_linked_list_task1_c/SLL.h)
    * [SLL.c](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab06_singly_linked_list_task1_c/SLL.c)
        * [Text file - double_numbers.txt](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab06_singly_linked_list_task1_c/double_numbers.txt)
* 1.2
    * [Source.c](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab06_singly_linked_list_task1.2_c/Source.c)
    * [StudentSLL.h](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab06_singly_linked_list_task1.2_c/StudentSLL.h)
    * [StudentSLL.c](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab06_singly_linked_list_task1.2_c/StudentSLL.c)

## LABORATORY - Modifications
[Download modification tasks in pdf.](https://gitlab.com/AleksaMCode/Programing2Course/raw/master/PII-Lab%20I%20dio%20-%20zadaci.pdf)
#### Recursion
1. [Write a recursive function to calculate nth element of modified Tribonacci sequence ( 1, 2, 3, 6, 11, ...) + optimize recursion using memoization](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A01-taks3_mod_tribonacci_sequence_recursion_c/Source.c)
2. [Calculate prime factors of a number n using recursion](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_rec_task2_c/Source.c)
3. [Reverse a number using recursion](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_rec_task3_c/Source.c)
4. [Using recursion determine whether a word is a palindrome](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_rec_task4_c/Source.c)
5. [Find the largest number in the array](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_rec_task5_c/Source.c)
6. [Calculate factorial using tail recursion](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_rec_task6_c/Source.c)
7. [Remove substring from string using recursion](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_rec_task7_c/Source.c)
8. [Remove a digit from a number using recursion](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_rec_task8_c/Source.c)
9. [Find the largest ASCII value in a string](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_rec_task9_c/Source.c)

#### Functions advanced concepts
1. [1.1 mod: Print out binary representation of an unsigned integer](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_func_advanced_concepts_task1_c/Source.c)
2. [1.1 mod: Print out reversed string](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_func_advanced_concepts_task2_c/Source.c)
3. [1.1 mod: Print out decimal part of a real number](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_func_advanced_concepts_task3_c/Source.c)
4. [1.1 mod: Print out a struct](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_func_advanced_concepts_task4_c/Source.c)
5. [1.1 mod: Print out a string x times](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_func_advanced_concepts_task5_c/Source.c)
6. [1.2 + find max. prime subarray](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_func_advanced_concepts_task6_c/Source.c)
7. [1.2 + find max. subarray of palindrome numbers](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_func_advanced_concepts_task7_c/Source.c)
8. [1.2 + find max fibonacci subarray](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_func_advanced_concepts_task8_c/Source.c)
9. [1.2 + find max subarray of numbers with increasing digits](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-Lab_modification_func_advanced_concepts_task9_c/Source.c)

# PRESENTATIONS
Solutions to the tasks, which are located at the end of a presentation.
#### P1 (A01) - Recursion
1. [Write a program that loads the (unsigned) number n, and then prints the sum of digits of n. Calculating the sum of digits should be done in recursive function](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A01-taks1_sum_of_digits_recursion_c/Source.c)
2. [Write a program that loads the (unsigned) number n, and then prints the digits of the number n starting with the msd. Digit printing should be done in recursive function. + Additional function : prints the digits of the number n starting with the lsd](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A01-taks2_print_out_digits_recursion_c/Source.c)
3. [Write a recursive function to calculate nth element of modified Tribonacci sequence ( 1, 2, 3, 6, 11, ...) + optimize recursion using memoization](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A01-taks3_mod_tribonacci_sequence_recursion_c/Source.c)
4. [Calculate factoradic using recursion](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A01-taks4_%20factoradics_recursion_c/Source.c)

#### P2 (A02) - Functions advanced concepts (Function with a variable number of arguments | Function pointers | Command line arguments)
1. [Write a function with a variable number of arguments that accounts product of any number of real numbers](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A02-taks1_product_of_numbers_var_arg_lists_c/Source.c)
2. [Write a function with variable number of arguments which forms a dynamic array based on the format (%d, %s, %f, %lf, %c)](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A02-taks2_create_dynamic_string_var_arg_lists_c/Myprintf.c)
3. [Extract decimal part from a floating point numbers using a function with function pointer as argument](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A02-taks3_get_decimal_part_func_pointers_c/Form.c)
4. [Find max. positiv and even subarray of numbers using a function with function pointer as argument](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A02-taks4_max_subarray_func_pointers_c/MaxSubarray.c)
5. [Write a program that checks whether the string 'substring' is located in a string 'string' at least n times. Strings and the number n are used as command line arguments](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A02-taks5_substring_problem_command_line_args_c/Source.c)

#### P3 (A03) - Text files
1. [Write a program that writes to a file, whose name is entered as a command line argument, enter the first n natural numbers (n is set from standard input), and then calculates the size of the file in bytes](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A03-task1_file_size_text_files_c/Source.c)
2. [Write a program that opens a text file, whose name is entered as the first command-line argument. Replaces all occurrences of the character referred to as the second command line argument with the character referred to as the third command line argument](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A03-task2_character_substitution_text_files_c/Source.c)
3. [Write a program that opens a text file, whose name is entered as the first command-line argument. Replaces all occurrences of the string referred to as the second command line argument with the string referred to as the third command line argument](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A03-task3_string_substitution_text_files_c/Source.c)
4. [Write a program that opens a text file, whose name is entered as the first command-line argument. Find and print all strings that are palindromes](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A03-task4_palindrome_text_files_c/Source.c)
 * [Solve 4th task using stack](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A03-task4_palindrome_text_files_STACK_c/Source.c)
5. [The file TROUGAO.TXT has data on a variety of triangles. Each row of the file contains data on a triangle in the form of: (x1, y1) (x2, y2) (x3, y3) where,<br />(x1, y1), (x2, y2) and (x3, y3) - integer coordinates of the vertices of a triangle, for example:<br />	(0,0) (4,0) (4,3)<br />	(1,0) (1,1) (2,4)<br />Write a program that reads from a file TRIANGLE.TXT data and finds the triangle with the largest surface area <br />and the triangle with the largest circumference, and store the result in a file MEASURE.TXT.](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A03-task5_text_files_c/Source.c)

#### P4 (A04) - Binary files
1. [Task 1 and task 2 solution](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A04-task1_task2_binary_files_c/Source.c)
2. [Task 1 and task 2 solution](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A04-task1_task2_binary_files_c/Source.c)
3. [Write a program in which you, from a binary file whose name is the first command-line argument, read (binary entered, unsorted) information about an unknown number of students and establish appropriate dynamic array, and then sort the set descending according to the average grade and formatte the print on standard output.<br />Data kept on the student are: the index number, surname, name and average grade (C library function - QSORT)](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/PII-A04-task3_binary_files_c/Source.c)

# Data Structures Library
#### Singly Linked Lists - SLL
1. SLL w/ void* key
 * [sll.h file](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/Singly_Linked_List_c/sll.h)
 * [sll.c file](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/Singly_Linked_List_c/sll.c)

#### Doubly Linked Lists - DLL
1. DLL w/ void* key
 * [dll.h file](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/Doubly_Linked_List_c/dll.h)
 * [dll.c file](https://gitlab.com/AleksaMCode/Programing2Course/blob/master/Doubly_Linked_List_c/dll.c)

# Helping
#### I found a bug!

If you found a bug send me an email.
Include steps to consistently reproduce the problem, actual vs. expected
results, screenshots.

# Contact info

* **E-mail:** 
 [Aleksa Majkić (aleksamcode@gmail.com)](mailto:aleksamcode@gmail.com)