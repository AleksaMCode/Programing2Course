/* Write a program in which u need to read from a file whose name is the first command-line argument,
read binary intered 4-byte data then print out first n numbers in ascending order, where n is
referred to as the second argument in the command line. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define SWAP(a, b) { a^=b; b^=a; a^=b; }

/* COMB SORT
Worst-case performance: O(n*n)
Best-case performance: O(n log n) */

void comb_sort(int a[], size_t a_size)
{
	unsigned gap = a_size; /* Initialize gap size */
	double shrink = 1.3f; /* Set the gap shrink factor */
#define bool int
#define false 0
#define true 1
	bool sorted = false;
	while (!sorted)
	{
		/* Update the gap value for a next comb */
		gap = (unsigned)floor(gap / shrink);
		if (gap > 1)
			sorted = true;
		else
			gap = 1,
			sorted = true; /* array isn't sorted as long as gap > 1 */

						   /* A single "comb" over the input list */
		unsigned i = 0;
		while (i + gap < a_size)
		{
			if (a[i] > a[i + gap])
			{
				SWAP(a[i], a[i + gap]);
				sorted = false;
				/* If this assignment never happens within the loop,
				then there have been no swaps and the list is sorted. */
			}
			i++;
		}
	}
}

int main(int argc, char* argv[])
{
	if (argc < 3)
		return printf("Too few arguments supplied!\n"), 1;

	FILE* input;
	if (atoi(argv[2]) > 5)
		return printf("File only has 5 numbers!\n"), 1;

	int* array, n = atoi(argv[2]);
	array = malloc(n * 4);

	if (!(input = fopen(argv[1], "rb")))
		return printf("There was an error opening the file '%s'", argv[1]), 1;

	fread(array, 4, 5, input);

	comb_sort(array, n);

	printf("Array in ascending order:\n");
	printf("%d,", array[0]);
	for (int i = 1; i < n; i++)
	{
		printf(" %d", array[i]);
		i != n - 1 ? printf(",") : printf("\n");
	}

	free(array);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */