#include "StudentSLL.h"

SLL* insertBeginning(SLL** phead, type data)
{
	struct sll* new;
	if ((new = malloc(sizeof(SLL))) == NULL)
		return NULL;

	new->key = data;
	(*new).next = *phead;
	*phead = new;
	return new;
}

void insertEnd(SLL** pnode, type data)
{
	SLL* tmp,
		*new = malloc(sizeof(SLL));
	if (!new)
		return;

	new->key = data;
	new->next = NULL;
	if (*pnode == NULL)
		*pnode = new;
	else
	{
		for (tmp = *pnode; tmp->next; tmp = tmp->next);
		tmp->next = new;
	}
}

void insertNodeAt(SLL* node, int place, type data)
{
	while (--place)
		node = node->next;
	insertBefore(node, data);
}

void insertBefore(SLL* node, type data)
{
	SLL* new = malloc(sizeof(SLL));
	if (!new)
		return;
	new->key = node->key;
	new->next = node->next;
	node->key = data;
	node->next = new;
}

void deleteNodeAt(SLL* node, int place)
{
	while (--place)
		node = node->next;
	deleteNode(node);
}

bool deleteNode(SLL* node)
{
	SLL* tmp = node->next;
	if (!tmp)
		return false;
	node->key = tmp->key;
	node->next = tmp->next;
	free(tmp);
	return true;
}

void freeList(SLL** phead)
{
	while (*phead)
	{
		SLL* tmp = (*phead)->next;
		free(*phead);
		*phead = tmp;
	}
	*phead = NULL;
}

SLL* search(SLL* head, char indxNum[])
{
	for (; head && strcmp(head->key.indexNumber, indxNum) != 0; head = (*head).next);
	return head;
}

void sort(SLL* head, short typeOfSort)
{
	for (; head && head->next; head = head->next)
	{
		SLL* min_max = head;
		for (SLL* j = head->next; j; j = j->next)
			if (typeOfSort == 2)
			{
				if (strcmp(min_max->key.indexNumber, j->key.indexNumber) > 0)
					min_max = j;
			}
			else
				if (strcmp(min_max->key.indexNumber, j->key.indexNumber) > 0)
					min_max = j;

		if (min_max != head)
		{
			type tmp = head->key;
			head->key = min_max->key;
			min_max->key = tmp;
		}
	}
}

void print(SLL* head)
{
	int i = 1;
	printf("=== =================== =================== ==========\n");
	printf("NO. FIRST NAME          LAST NAME           NO.INDEX\n");
	printf("=== =================== =================== ==========\n");
	while (head)
		printf("%2d. %-19s %-19s %-10s\n", i++, head->key.fname, head->key.lname, head->key.indexNumber), head = head->next;
	printf("=== =================== =================== ==========\n");
}

void invert(SLL** phead)
{
	SLL* first, *rest;
	if (*phead == NULL)
		return;

	first = *phead, rest = (*phead)->next;
	if (!rest)
		return;

	invert(&rest);

	first->next->next = first;
	first->next = NULL;
	*phead = rest;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */