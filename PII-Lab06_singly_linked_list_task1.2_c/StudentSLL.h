#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct st
{
	char fname[19],
		lname[19],
		indexNumber[10];
}STUDENT;

typedef STUDENT type;

typedef int bool;
#define true 1
#define false 0

typedef struct sll
{
	type key;
	struct sll* next;
}SLL;

SLL* insertBeginning(SLL** phead, type data);
void insertEnd(SLL** pnode, type data);
void insertNodeAt(SLL* node, int place, type data);
void insertBefore(SLL* node, type data);
void deleteNodeAt(SLL* node, int place);
bool deleteNode(SLL* node);
void freeList(SLL** phead);
SLL* search(SLL* head, char indxNum[]);
void print(SLL* head);
void sort(SLL* head, short typeOfSort);
void invert(SLL** phead);

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */