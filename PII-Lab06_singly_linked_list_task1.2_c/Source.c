#include "StudentSLL.h"

void addStudent(STUDENT* st)
{
	printf("--------------------------------\n");
	printf("FIRST NAME: "), scanf("%s", st->fname);
	printf("LAST NAME: "), scanf("%s", st->lname);
	printf("INDEX NO.: "), scanf("%s", st->indexNumber);
	printf("--------------------------------\n");
}

int main()
{
	char answer[5], pos[5];
	strcpy(pos, "0");
	int addCount = 0;
	SLL* list = NULL;
	do {
		system("CLS"), printf("ADD/UPDATE [A]\nDELETE [DEL]\nSEARCH [SEA]\nPRINT [PR]\nSORTING [S]\nINVERT [INV]\nEXIT [END]\n\n>> "), scanf("%s", answer);
		if (strcmp(answer, "END"))
		{
			if (!strcmp(answer, "A"))
			{
				STUDENT st;
				do system("CLS"), printf("ADD: BEGINNING [beg] | END [end] | POSITION [pos]\n>> "), scanf("%s", answer); while (strcmp(answer, "beg") != 0 && strcmp(answer, "end") != 0 && strcmp(answer, "pos") != 0);
				if (!strcmp(answer, "pos"))
					printf("Enter position: "), scanf("%s", pos);

				if (!strcmp(answer, "beg") || atoi(pos) == 1) addStudent(&st), insertBeginning(&list, st), addCount++;
				else if (!strcmp(answer, "end") || atoi(pos) == addCount + 1) addStudent(&st), insertEnd(&list, st), addCount++;
				else if (!strcmp(answer, "pos"))
				{
					if (atoi(pos) <= addCount)
						addStudent(&st), insertNodeAt(list, atoi(pos), st), addCount++;
					else printf("Position doesn't exist!\n"), system("PAUSE");
				}
				strcpy(pos, "0");
			}
			else if (!strcmp(answer, "DEL"))
			{
				do system("CLS"), printf("DELETE: ALL [all] | POSITION [pos]\n>> "), scanf("%s", answer); while (strcmp(answer, "all") != 0 && strcmp(answer, "pos") != 0);
				if (!strcmp(answer, "all")) freeList(&list);
				else if (!strcmp(answer, "pos"))
				{
					do printf("Enter position: "), scanf("%s", pos); while (atoi(pos) >= addCount);
					deleteNodeAt(list, atoi(pos));
				}
				else printf("Position doesn't exist!\n"), system("PAUSE");
			}
			else if (!strcmp(answer, "SEA"))
			{
				char index[10];
				do system("CLS"), printf("Index number: "), scanf("%s", index); while (strlen(index) >= 9);
				SLL* tmp = search(list, index);
				if (tmp) printf("Index %s belongs to %s %s\n", index, tmp->key.fname, tmp->key.lname);
				else printf("Index number %s doesn't exist in the list!\n", index);
				system("PAUSE");
			}
			else if (!strcmp(answer, "PR")) print(list), system("PAUSE");
			else if (!strcmp(answer, "S"))
			{
				int typeOfSort;
				do system("CLS"), printf("SORT: DESCENDING [1] | ASCENDING [2]\n>> "), scanf("%d", &typeOfSort); while (typeOfSort != 1 && typeOfSort != 2);
				sort(list, (short)typeOfSort);
			}
			else if (!strcmp(answer, "INV")) invert(&list);
		}
	} while (strcmp(answer, "END") != 0);

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */