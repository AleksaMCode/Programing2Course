/* Calculate largest digit in a number. */
#include <stdio.h>
#include <math.h>

int max_digit(unsigned n)
{
	return !n ? 0 : (int)fmax(n % 10, (double)max_digit(n / 10));
}

int main()
{
	unsigned n;
	printf("Enter number: "), scanf("%u", &n);
	printf("Largest digit in a number %u is %d.\n", n, max_digit(n));
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */