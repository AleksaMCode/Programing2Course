/* Write a program which must loade from standard input data for n students and form
appropriate dynamic array, and then loaded set of data on students write in binary file
whose named is first command line argument. Data kept on the student are: last name, first name and average grade. */

#include <stdio.h>
#include <stdlib.h>

typedef struct { char last_name[20], first_name[20]; double avg_grade; }STUDENT;

void read(STUDENT* st) {
	printf("LAST NAME: "); scanf("%s", st->last_name);
	printf("FIRST NAME: "); scanf("%s", st->first_name);
	printf("AVERAGE GRADEF: "); scanf("%lf", &st->avg_grade);
}

int main(int argc, char* argv[]) 
{
	if (argc < 2)
		return printf("Too few arguments supplied!\n"), 1;

	STUDENT* array;
	FILE* output;
	int n;
	do { printf("How many students do u want to enter? "), scanf("%d", &n); } while (n < 1);
	array = malloc(n * sizeof(STUDENT));

	for (int i = 0; i < n; i++)
		system("CLS"), printf("%d. STUDENT:\n", i + 1), read(array + i);

	if (!(output = fopen(argv[1], "wb")))
		return printf("There was an error opening the file '%s'", argv[1]), 1;

	fwrite(array, sizeof(STUDENT), n, output);
	
	if (ferror(output)) return printf("There was an error reading data from a file '%s'.\n", *(argv + 1)), 1;
	if (fclose(output)) return printf("File was unsuccessfully closed.\n"), 1;

	free(array);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */