/* Write a program that loads the (unsigned) number n, and then prints the digits of the number n starting with the msd. 
Digit printing should be done in recursive function. + Additional function : prints the digits of the number n starting with the lsd. */
#include <stdio.h>

void digit_msd(unsigned n) 
{
	if (n / 10) digit_msd(n / 10);
	printf("%u", n % 10);
}

/* additional function */
void digit_lsd(unsigned n) 
{
	printf("%u", n % 10);
	if (n / 10) digit_lsd(n / 10);
}

int main() 
{
	unsigned n;
	printf("Enter number: "), scanf("%u", &n);

	printf("Digits, from most to least significant digit\n");
	digit_msd(n);
	/* additional function */
	printf("\nDigits, from least to most significant digit\n");
	digit_lsd(n);

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */