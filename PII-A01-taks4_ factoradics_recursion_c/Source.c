/* Calculate factoradic using recursion. */
#include<stdio.h>
#include<math.h>
/*Factorial numeral system: https://oeis.org/wiki/Factorial_numeral_system */
/* n=13 rez: 201 */
/* n=50 rez: 2010 */

unsigned factorial(unsigned n)
{
	return (!n || n == 1) ? 1 : n * factorial(n - 1);
}

int i = 1, digit, fact;

void factoradic(unsigned n)
{
	if (!n)
	{
		printf("Factoradic is: %d\n", fact);
		return;
	}

	while (factorial(i) <= n) i++; i--;
	digit = n / factorial(i);
	fact += digit * (int)pow(10, i - 1);
	factoradic(n % factorial(i));
}

int main()
{
	unsigned n;
	printf("Enter 1st n="), scanf("%u", &n);
	factoradic(n);
	i = 1, digit = fact = 0;
	printf("Enter 2nd n="), scanf("%u", &n);
	factoradic(n);

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */