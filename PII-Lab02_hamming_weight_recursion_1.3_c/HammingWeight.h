#ifndef HAMMINGWEIGHT_H
#define HAMMINGWEIGHT_H
#include <stdio.h>

int hamming_weight(unsigned);

#endif // !HAMMINGWEIGHT_H

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */