/* Calculate Hamming weight (population count, popcount, or sideways sum) for one unsigned number. */
#include "HammingWeight.h"

int main()
{
	unsigned n;
	printf("n= "), scanf("%u", &n);
	printf("Hamming weight for unsigned number %u is %d.\n", n, hamming_weight(n));
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */