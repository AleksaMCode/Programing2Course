#include "HammingWeight.h"

int hamming_weight(unsigned n)
{
	return !n ? 0 : (n & 1) + hamming_weight(n >> 1);
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */