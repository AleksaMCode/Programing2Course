/* Write a program that loads the (unsigned) number n, and then prints the sum of digits of n. 
Calculating the sum of digits should be done in recursive function. */
#include <stdio.h>

unsigned sum_of_digits(unsigned n)
{
	if (!(n / 10)) return n;
	else return n % 10 + sum_of_digits(n / 10);
}

int main()
{
	unsigned n;
	printf("Enter number: "), scanf("%u", &n);
	printf("Digits sum: %u\n", sum_of_digits(n));
	/* The digit sum of a given integer is the sum of all its digits. */

	return 0;
}

/* mailto:aleksa */ 
/* Copyright(C) 2017 Aleksa Majkic */