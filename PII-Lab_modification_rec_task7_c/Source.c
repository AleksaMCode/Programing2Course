/* Remove substring from string using recursion. */
#include <stdio.h>
#include <string.h>

void eject(char* a, char* b)
{
	if (a)
		memmove(a, a + strlen(b), 1 + strlen(a + strlen(b)));
	else
		return;
	eject(strstr(a, b), b);
}

int main()
{
	char string[50], substring[50];
	printf("Enter -> string substring: "), scanf("%s %s", string, substring);
	eject(strstr(string, substring), substring);
	printf("RESULT: %s", string);

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */