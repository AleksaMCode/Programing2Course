/* 1st midterm exam - 1st task - 22.04.2015. - GROUP A */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void form(char** a, char** b, int n, char*(*t)(char*))
{
	for (int i = 0; i < n; i++)
		b[i] = (*t)(a[i]);
}

char* reverse(char* word)
{
	char* rstring = calloc(strlen(word) + 1, 1);
	strcpy(rstring, word);
	_strrev(rstring);
	return rstring;
}

int main(int argc, char* argv[])
{
	if (argc < 1)
		return printf("Too few arguments supplied!\n"), 1;

	FILE* input, *output;
	char** a, **b, next[50];
	int len = 10, n = 0;
	
	if (!(input = fopen(argv[1], "r"))) return printf("There was an error opening the file '%s'", argv[1]), 1;
	if (!(output = fopen(argv[2], "w"))) return printf("There was an error opening the file '%s'", argv[2]), 1;
	
	a = (char**)malloc(len * sizeof(char*));

	while (fscanf(input, "%s", next) != EOF) 
	{
		if (n == len) 
			a = (char**)realloc(a, (len *= 2) * sizeof(char*));

		a[n] = calloc(strlen(next) + 1, 1);
		strcpy(a[n++], next);
	}
	
	if (fclose(input))
		printf("File was unsuccessfully closed.\n");

	b = (char**)malloc(n * sizeof(char*));
	form(a, b, n, &reverse);

	for (int i = 0; i < n; i++)
		fprintf(output, "%s\n", b[i]), free(a[i]), free(b[i]);

	if (fclose(output))
		printf("File was unsuccessfully closed.\n");

	free(a), free(b);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2016 Aleksa Majkic */