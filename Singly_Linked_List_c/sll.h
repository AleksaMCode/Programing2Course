#pragma once
#include <stdio.h>
#include <stdlib.h>

/* Singly linked list library */
typedef void* type;

typedef int bool;
#define true 1
#define false 0

typedef struct sll
{
	type key;
	struct sll* next;
}SLL;

SLL* insertBeginning(SLL** phead, type data); /* 'phead' = 'head pointer' */
void insertEnd(SLL** pnode, type data);
void insertAfter(SLL* node, type data);
void insertBefore(SLL* node, type data);
bool deleteAfter(SLL* node);
bool deleteNode(SLL* node);
bool removeElement(SLL** phead, type data); /* remove using findElement func. */
bool removeElementVer2(SLL** phead, type data); /* search + delete */
void freeList(SLL** phead);
SLL* findElement(SLL* head, type data); /* find or search */
SLL* findElementVer2(SLL* head, type data);
SLL* findElementRecursive(SLL* head, type data);
SLL* searchSortedList(SLL* head, type data); /* for ascending list */
void selectionSort(SLL* head);
bool addSort(SLL** phead, type data);
bool addSortVer2(SLL** phead, type data);
void invert(SLL** phead); /* invert or reverse */
void invertRecursive(SLL* node, SLL** phead);
void reverseRecursive(SLL** phead);
void print(SLL* head);
void printRecursive(SLL* head);
void printRecursiveReverse(SLL* head);
int countNodes(SLL* head);
void appendToList(SLL** plist1, SLL** plist2);

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */