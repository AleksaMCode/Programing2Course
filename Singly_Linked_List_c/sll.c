#include "sll.h"

SLL* insertBeginning(SLL** phead, type data)
{
	struct sll* new;
	if ((new = malloc(sizeof(SLL))) == NULL)
		return NULL;

	new->key = data;
	(*new).next = *phead;
	*phead = new;
	return new;
}

void insertEnd(SLL** pnode, type data)
{
	SLL* tmp,
		*new = malloc(sizeof(SLL));
	if (!new)
		return;

	new->key = data;
	new->next = NULL;
	if (*pnode == NULL)
		*pnode = new;
	else
	{
		for (tmp = *pnode; tmp->next; tmp = tmp->next);
		tmp->next = new;
	}
}

void insertAfter(SLL* node, type data)
{
	SLL* new = malloc(sizeof(SLL));
	if (!new)
		return;
	new->key = data;
	new->next = node->next;
	node->next = new;
}

void insertBefore(SLL* node, type data)
{
	SLL* new = malloc(sizeof(SLL));
	if (!new)
		return;
	new->key = node->key;
	new->next = node->next;
	node->key = data;
	node->next = new;
}

bool deleteAfter(SLL* node)
{
	SLL* tmp = node->next;
	if (!tmp)
		return false;
	node->next = tmp->next;
	free(tmp);
	return true;
}

bool deleteNode(SLL* node)
{
	SLL* tmp = node->next;
	if (!tmp)
		return false;
	node->key = tmp->key;
	node->next = tmp->next;
	free(tmp);
	return true;
}

bool removeElement(SLL** phead, type data)
{
	SLL* tmp, *ptmp;
	if ((tmp = findElement(*phead, data)) == NULL)
		return false;

	if (tmp == *phead)
	{
		ptmp = (*phead)->next;
		free(*phead);
		*phead = ptmp;
	}
	else
	{
		for (ptmp = *phead; (*ptmp).next != tmp; ptmp = (*ptmp).next);
		ptmp->next = tmp->next;
		free(tmp);
	}
	return true;
}

bool removeElementVer2(SLL** phead, type data)
{
	SLL* tmp;
	for (; *phead && (*phead)->key != data; phead = &((*phead)->next));
	if (*phead)
	{
		tmp = *phead;
		*phead = (*phead)->next;
		free(tmp);
		return true;
	}
	else
		return false;
}

void freeList(SLL** phead)
{
	while (*phead)
	{
		SLL* tmp = (*phead)->next;
		free(*phead);
		*phead = tmp;
	}
	*phead = NULL;
}

SLL* findElement(SLL* head, type data)
{
	while (head && head->key != data)
		head = head->next;
	return head;
}

SLL* findElementVer2(SLL* head, type data)
{
	for (; head && head->key != data; head = (*head).next);
	return head;
}

SLL* findElementRecursive(SLL* head, type data)
{
	if (!head)
		return NULL;
	if (head->key == data)
		return head;
	return findElementRecursive(head->next, data);
}

SLL* searchSortedList(SLL* head, type data)
{
	for (; head && head->key < data; head = head->next);
	if (!head || head->key > data)
		return NULL;
	return head;
}

void selectionSort(SLL* head)
{
	for (; head && head->next; head = head->next)
	{
		SLL* min = head;
		for (SLL* j = head->next; j; j = j->next)
			if (min->key > j->key)
				min = j;
		if (min != head)
		{
			type tmp = head->key;
			head->key = min->key;
			min->key = tmp;
		}
	}
}

bool addSort(SLL** phead, type data)
{
	SLL* tmp,
		*new;
	if ((new = malloc(sizeof(SLL))) == NULL)
		return false;

	new->key = data;
	if (*phead == NULL || (*(*phead)).key >= data)
	{
		new->next = *phead;
		*phead = new;
	}
	else
	{
		for (tmp = *phead; tmp->next && (tmp->next)->key < data; tmp = tmp->next);
		new->next = tmp->next;
		tmp->next = new;
	}
	return true;
}

bool addSortVer2(SLL** phead, type data)
{
	SLL* new = malloc(sizeof(SLL));
	if (!new)
		return false;

	new->key = data;
	for (; *phead && (*phead)->key < data; phead = &((*phead)->next));
	new->next = *phead;
	*phead = new;
	return true;
}

void invert(SLL** phead)
{
	SLL* p1 = *phead, *p2 = (void*)0, *p3;
	while (p1)
	{
		p3 = p1->next;
		p1->next = p2;
		p2 = p1;
		p1 = p3;
	}
	*phead = p2;
}

void invertRecursive(SLL* node, SLL** phead)
{
	if (node == NULL)
	{
		*phead = node;
		return;
	}
	invertRecursive(node->next, phead);
	node->next->next = node;
	node->next = NULL;
}

void reverseRecursive(SLL** phead)
{
	SLL* first, *rest;
	if (*phead == NULL)
		return;

	first = *phead, rest = (*phead)->next;
	if (!rest)
		return;

	reverseRecursive(&rest);

	first->next->next = first;
	first->next = NULL;
	*phead = rest;
}

void print(SLL* head)
{
	for (; head; head = head->next);
	printf("On address %p is %<fmt> which point to address %p\n", head, head->key, head->next);
}

void printRecursive(SLL* head)
{
	if (!head)
		return;
	printf("%<fmt> ", head->key);
	printRecursive(head->next);
}

void printRecursiveReverse(SLL* head)
{
	if (!head)
		return;
	printRecursiveReverse(head->next);
	printf("%<fmt> ", head->key);
}

int countNodes(SLL* head)
{
	int count = 0;
	while (head)
		count++, head = head->next;
	return count;
}

void appendToList(SLL** plist1, SLL** plist2)
{
	while ((*plist1)->next)
		*plist1 = (*plist1)->next;
	(*plist1)->next = *plist2;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */