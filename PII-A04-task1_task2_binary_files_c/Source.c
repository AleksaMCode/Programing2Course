#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct { char index[10], first_name[20], surname[20]; double average_grade; }STUDENT;

void insert_student(STUDENT* st)
{
	printf("FIRST NAME: "), scanf("%s", st->first_name);
	printf("SURNAME: "), scanf("%s", st->surname);
	printf("NUM. of INDEX: "), scanf("%s", st->index);
	printf("AVERAGE GRADE: "), scanf("%lf", &st->average_grade);
}

int main(int argc, char* argv[]) {
	if (argc < 2)
		return printf("Too few arguments supplied!\n"), 1;

	FILE* input;
	STUDENT* array;
	int n;
	do printf("How many students do u want to enter? "), scanf("%d", &n); while (n < 0);
	if (!n) return 1;

	if (!(input = fopen(argv[1], "wb")))
		return printf("There was an error opening the file '%s'", argv[1]), 1;

	array = malloc(sizeof(STUDENT) * n);
	for (int i = 0; i < n; i++)
		printf("%d. STUDENT:\n", i + 1), insert_student(array + i), printf("\n");

	fwrite(array, sizeof(STUDENT), n, input);

	if (ferror(input)) return printf("There was an error reading data from a file '%s'.\n", *(argv + 1)), 1;
	if (fclose(input)) printf("File was unsuccessfully closed.\n");

	free(array);

	/* 2. task */
	if (!(input = fopen(argv[1], "rb")))
		return printf("There was an error opening the file '%s'", argv[1]), 1;

	array = malloc(sizeof(STUDENT) * n);
	fread(array, sizeof(STUDENT), n, input);
	printf("\n==== ==================== ==================== ========== =====\n");
	printf("NUM. FIRST NAME           LAST NAME             N.INDEX   GRADE\n");
	printf("==== ==================== ==================== ========== =====\n");
	for (int i = 0; i < n; i++)
		printf("%3d. %-20s %-20s %-10s %5.2lf\n", i + 1, array[i].first_name, array[i].surname, array[i].index, array[i].average_grade);
	printf("==== ==================== ==================== ========== =====\n");

	if (fclose(input))
		printf("File was unsuccessfully closed.\n");

	free(array);
	return 0;
}