/* Add option: my_printf("%z", s); where s is a struct */
#include <stdio.h>
#include <stdarg.h>

typedef struct { char fname[20], lname[20], index_number[8]; }STUDENT;

void my_printf(char* format, ...)
{
	va_list args;
	va_start(args, format);
	for (int i = 0; format[i]; i++)
	{
		if (format[i] == '%' && format[i + 1] == 'z')
		{
			STUDENT s = va_arg(args, STUDENT);
			printf("[%s] %s %s", s.index_number, (*(&s)).fname, (&s)->lname);
			i++;
		}
		else if (format[i] == '%' && format[i + 1] == 'd')
		{
			printf("%d", va_arg(args, int));
			i++;
		}
		else if (format[i] == '%' && format[i + 1] == 'c')
		{
			printf("%c", va_arg(args, char));
			i++;
		}
		else if (format[i] == '%' && format[i + 1] == 's')
		{
			printf("%s", va_arg(args, char*));
			i++;
		}
		else if (format[i] == '%' && format[i + 1] == 'f')
		{
			printf("%f", va_arg(args, float));
			i++;
		}
		else if (format[i] == '%' && format[i + 1] == 'l' && format[i + 2] == 'f')
		{
			printf("%lf", va_arg(args, double));
			i += 2;
		}
		else
		{
			printf("%c", format[i]);
		}
	}
	va_end(args);
}


int main()
{
	STUDENT s = { "Marko", "Markovic", "1111/11" };
	my_printf("%z", s);
	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */