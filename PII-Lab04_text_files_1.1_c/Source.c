#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[])
{
	if (argc < 3)
		printf("Too few arguments supplied!\n");
	else
	{
		FILE* input, *output;
		if (!(input = fopen(argv[1], "r")))
			return printf("There was an error opening the file '%s'", argv[1]), 1;
		if (!(output = fopen(argv[2], "w")))
			return printf("There was an error opening the file '%s'", argv[2]), 1;

		char k, write[1000] = { 0 }, max[100] = { 0 };
		int i = 0;
		while ((k = fgetc(input)) != EOF)
		{
			if (k >= 0x41 && k <= 0x5A || k >= 0x61 && k <= 0x7A)
				write[i++] = k;
			else if (i)
			{
				write[i++] = '\n';
				write[i] = '\0';
				fputs(write, output);
				write[--i] = 0;
				i = 0;

				if (strlen(write) > strlen(max))
					strcpy(max, write);
			}
		}

		printf("Longest word: %s\n", max);

		if (fclose(input)) printf("File was unsuccessfully closed.\n");
		if (fclose(output)) printf("File was unsuccessfully closed.\n");
	}

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */