/* Calculate factorial using tail recursion. */
#include <stdio.h>

int fact_helper(int n, int acc)
{
	if (!n)
		return acc;
	return fact_helper(n - 1, acc * n);
}

int factoriel(int n)
{
	return fact_helper(n, 1);
}

int main()
{
	int n;
	do printf("Enter a number: "), scanf("%d", &n); while (n < 0);
	printf("%d!=%d", n, factoriel(n));

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */