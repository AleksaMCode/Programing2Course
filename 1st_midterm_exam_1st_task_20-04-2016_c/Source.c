/* 1st midterm exam - 1st task - 20.04.2016. - GROUP A */
#include <stdio.h>
#include <stdarg.h>
#include <math.h>

void f(double x, int n, ...) {
	printf("%5.3lf", x);
	va_list arg;
	va_start(arg, n);

	for (int i = 0; i < n; i++)
	{
		double(*p)(double) = va_arg(arg, double*);
		printf("   %.7lf", (*p)(x));
	}
	printf("\n===== ");
	int i = 0;
	while (i < n) printf("  ========= "), i++;
	va_end(arg);
}

int main()
{
	double x;
	do printf("Enter a number [-1,1): "), scanf("%lf", &x); while (x < -1 || x >= 1);
	f(x, 3, &sin, &cos, &exp);

	return 0;
}

/* mailto:aleksa */
/* Copyright(C) 2016 Aleksa Majkic */